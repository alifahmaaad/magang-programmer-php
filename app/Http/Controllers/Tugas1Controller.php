<?php

namespace App\Http\Controllers;

use App\Models\Ganjilgenap;
use App\Models\hitungvokal;
use App\Models\Kalkulator;
use Illuminate\Http\Request;


class Tugas1Controller extends Controller
{
    public function index()
    {
        return view('helloworld');
    }
    public function ganjilgenap()
    {
        $history = Ganjilgenap::all();
        return view('ganjilgenap', ['histori' => $history]);
    }
    public function kalkulator()
    {
        $history = Kalkulator::all();
        return view('kalkulator', ['histori' => $history]);
    }
    public function hitungvokal()
    {
        $history = hitungvokal::all();
        return view('hitungvokal', ['histori' => $history]);
    }
    public function postganjilgenap(Request $request)
    {
        $this->validate($request, [
            'bil1' => 'required|numeric',
            'bil2' => 'required|numeric|min:bil1',
        ]);
        $a = $request->bil1;
        $b = $request->bil2;
        $output = "";
        for ($i = $a; $i <= $b; $i++) {
            if ($i % 2 != 0) {
                $output .= "Angka $i adalah Ganjil \n";
            } else {
                $output .= "Angka $i adalah Genap \n";
            }
        }
        Ganjilgenap::create([
            'bilangan_1' => $request->bil1,
            'bilangan_2' => $request->bil2,
            'output' => $output
        ]);
        return redirect()->route('ganjilgenap');
    }
    public function posthitungvokal(Request $request)
    {
        $this->validate($request, [
            'text' => 'required|min:5|max:20',
        ]);
        $str = $request->text;
        $lowstr = strtolower($str);
        $sumvokal = 0;
        $status = "";
        $output = "";
        for ($i = 0; $i < strlen($lowstr); $i++) {
            if ($lowstr[$i] == "a") {
                if (strpos($status, "a") === false) {
                    $status .= "a ";
                }
                $sumvokal += 1;
            } else if ($lowstr[$i] == "i") {
                if (strpos($status, "i") === false) {
                    $status .= "i ";
                }
                $sumvokal += 1;
            } else if ($lowstr[$i] == "u") {
                if (strpos($status, "u") === false) {
                    $status .= "u ";
                }
                $sumvokal += 1;
            } else if ($lowstr[$i] == "e") {
                if (strpos($status, "e") === false) {
                    $status .= "e ";
                }
                $sumvokal += 1;
            } else if ($lowstr[$i] == "o") {
                if (strpos($status, "o") === false) {
                    $status .= "o ";
                }
                $sumvokal += 1;
            }
        }
        if (strlen($status) > 2) {
            $a = strlen($status) - 2;
            $rep = "dan ";
            $status = substr_replace($status, $rep, $a, 0);
        }
        $output = "\"$str\" = $sumvokal yaitu $status ";

        hitungvokal::create([
            'text' => $request->text,
            'output' => $output
        ]);
        return redirect()->route('hitungvokal');
    }
    public function postkalkulator(Request $request)
    {
        $this->validate($request, [
            'operator' => 'required',
            'input' => 'required',
        ]);
        $str = $request->input;
        $operator = $request->operator;
        $hasil = "";
        $op =  "";
        for ($i = 0; $i < strlen($str); $i++) {
            if (($str[$i] == "+") || ($str[$i] == "-") || ($str[$i] == "X") || ($str[$i] == "x") || ($str[$i] == "/")) {
                $op = $str[$i];
            }
        }
        if ($op != $operator) {
            return redirect()->back()->withErrors(['Operator pada string tidak sama dengan pilihan operator (tombol)']);
        }
        $str = str_replace(" ", "", $str);
        $str = str_replace("+", " ", $str);
        $str = str_replace("-", " ", $str);
        $str = str_replace("X", " ", $str);
        $str = str_replace("/", " ", $str);
        $bil = explode(" ", $str);

        if ($operator == "+") {
            $hasil = $bil[0] + $bil[1];
        } else if ($operator == "-") {
            $hasil = $bil[0] - $bil[1];
        } else if ($operator == "X" || $operator == "x") {
            $hasil = $bil[0] * $bil[1];
        } else if ($operator == "/") {
            if ($bil[1] != 0) {
                $hasil = $bil[0] / $bil[1];
            } else {
                $hasil = "tidak bisa dilakukan";
            }
        }

        Kalkulator::create([
            'input' => $request->input,
            'operator' => $request->operator,
            'output' => $hasil
        ]);
        return redirect()->route('kalkulator');
    }
}
