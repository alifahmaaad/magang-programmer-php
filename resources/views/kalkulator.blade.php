<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
    <title>Kalkulator</title>
</head>

<body>
    <div class="container pt-5">
        <h1>Kalkulator Sederhana</h1>
        @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <ul>{{ $error }}</ul>
                @endforeach
            </ul>
        </div>
        @endif

        <form action="/kalkulatorpost" method="post">
            {{ csrf_field() }}

            <div class="form-group pt-2">
                <label for="input">Input String</label>
                <input class="form-control" type="text" name="input">
            </div>
            <div class="btn-group pt-3 " role="group" aria-label="Basic radio toggle button group">
                <input type="radio" class="btn-check" name="operator" value="+" id="operator1" autocomplete="off" checked>
                <label class="btn btn-outline-secondary" for="operator1">+</label>

                <input type="radio" class="btn-check" name="operator" value="-" id="operator2" autocomplete="off">
                <label class="btn btn-outline-secondary" for="operator2">-</label>

                <input type="radio" class="btn-check" name="operator" value="X" id="operator3" autocomplete="off">
                <label class="btn btn-outline-secondary" for="operator3">X</label>

                <input type="radio" class="btn-check" name="operator" value="/" id="operator4" autocomplete="off">
                <label class="btn btn-outline-secondary" for="operator4">/</label>
            </div>
            <div class="form-group pt-3">
                <input class="btn btn-primary" type="submit" value="Hitung">
            </div>
        </form>


        <div class="pt-5">
            <h3>History</h3>
            <table class="table ">
                <thead>
                    <tr>
                        <th scope="col">No</th>
                        <th scope="col">Input</th>
                        <th scope="col">Operator</th>
                        <th scope="col">Hasil</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($histori as $his)
                    <tr>
                        <th scope="row">{{$loop->iteration}}</th>
                        <td>{{$his->input}}</td>
                        <td>{{$his->operator}}</td>
                        <td>{{$his->output}}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</body>

</html>