<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
    <title>Mencari Ganjil Genap</title>
</head>

<body>
    <div class="container pt-5">
        <h1>Mencari Ganjil Genap</h1>
        @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <ul>{{ $error }}</ul>
                @endforeach
            </ul>
        </div>
        @endif
        <form action="/ganjilgenappost" method="post">
            {{ csrf_field() }}

            <div class="form-group pt-2">
                <label for="bil1">Bilangan 1</label>
                <input class="form-control" type="number" name="bil1">
            </div>
            <div class="form-group pt-2">
                <label for="bil2">Bilangan 2</label>
                <input class="form-control" type="number" name="bil2">
            </div>
            <div class="form-group pt-2">
                <input class="btn btn-primary" type="submit" value="Proses">
            </div>
        </form>
        <div class="pt-5">
            <h3>History</h3>
            <table class="table ">
                <thead>
                    <tr>
                        <th scope="col">No</th>
                        <th scope="col">Bilangan 1</th>
                        <th scope="col">Bilangan 2</th>
                        <th scope="col">Output</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($histori as $his)
                    <tr>
                        <th scope="row">{{$loop->iteration}}</th>
                        <td>{{$his->bilangan_1}}</td>
                        <td>{{$his->bilangan_2}}</td>
                        <td>{{$his->output}}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</body>

</html>