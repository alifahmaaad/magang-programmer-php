<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
    <title>Menghitung Jumlah Vokal Pada kalimat</title>
</head>

<body>
    <div class="container pt-5">
        <h1>Menghitung Alphabet Vokal</h1>
        @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <ul>{{ $error }}</ul>
                @endforeach
            </ul>
        </div>
        @endif
        <form action="/hitungvokalpost" method="post">
            {{ csrf_field() }}

            <div class="form-group pt-2">
                <label for="text">Text</label>
                <textarea class="form-control" type="text" name="text" style="height: 100px"></textarea>
            </div>
            <div class="form-group pt-2">
                <input class="btn btn-primary" type="submit" value="Hitung">
            </div>
        </form>
        <div class="pt-5">
            <h3>History</h3>
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">No</th>
                        <th scope="col">Text</th>
                        <th scope="col">Output</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($histori as $his)
                    <tr>
                        <th scope="row">{{$loop->iteration}}</th>
                        <td>{{$his->text}}</td>
                        <td>{{$his->output}}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</body>

</html>