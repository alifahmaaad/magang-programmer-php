use sdm;

INSERT INTO `departemen` ( `nama`) VALUES ( 'Manajemen');
INSERT INTO `departemen` ( `nama`) VALUES ( 'Pengembangan Bisnis');
INSERT INTO `departemen` ( `nama`) VALUES ( 'Teknisi');
INSERT INTO `departemen` ( `nama`) VALUES ( 'Analis');

INSERT INTO `karyawan` ( `nama`, `jenis_kelamin`, `status`, `tanggal_lahir`, `tanggal_masuk`, `departemen`) VALUES ( 'Rizki Saputra', 'L', 'Menikah', '1980-10-11', '2011-01-01', '1');
INSERT INTO `karyawan` ( `nama`, `jenis_kelamin`, `status`, `tanggal_lahir`, `tanggal_masuk`, `departemen`) VALUES ( 'Farhan Reza', 'L', 'Belum', '1989-11-1', '2011-01-01', '1');
INSERT INTO `karyawan` ( `nama`, `jenis_kelamin`, `status`, `tanggal_lahir`, `tanggal_masuk`, `departemen`) VALUES ( 'Riyando Adi', 'L', 'Menikah', '1977-1-25', '2011-01-01', '1');
INSERT INTO `karyawan` ( `nama`, `jenis_kelamin`, `status`, `tanggal_lahir`, `tanggal_masuk`, `departemen`) VALUES ( 'Diego Manuel', 'L', 'Menikah', '1983-2-22', '2012-09-04', '2');
INSERT INTO `karyawan` ( `nama`, `jenis_kelamin`, `status`, `tanggal_lahir`, `tanggal_masuk`, `departemen`) VALUES ( 'Satya Laksana', 'L', 'Menikah', '1981-1-12', '2011-03-19', '2');
INSERT INTO `karyawan` ( `nama`, `jenis_kelamin`, `status`, `tanggal_lahir`, `tanggal_masuk`, `departemen`) VALUES ( 'Miguel Hernandez', 'L', 'Menikah', '1994-10-16', '2014-06-15', '2');
INSERT INTO `karyawan` ( `nama`, `jenis_kelamin`, `status`, `tanggal_lahir`, `tanggal_masuk`, `departemen`) VALUES ( 'Putri Persada', 'P', 'Menikah', '1988-1-30', '2013-04-14', '2');
INSERT INTO `karyawan` ( `nama`, `jenis_kelamin`, `status`, `tanggal_lahir`, `tanggal_masuk`, `departemen`) VALUES ( 'Alma Safira', 'P', 'Menikah', '1991-5-18', '2013-09-28', '3');
INSERT INTO `karyawan` ( `nama`, `jenis_kelamin`, `status`, `tanggal_lahir`, `tanggal_masuk`, `departemen`) VALUES ( 'Haqi Hafiz', 'L', 'Belum', '1995-9-19', '2015-03-09', '3');
INSERT INTO `karyawan` ( `nama`, `jenis_kelamin`, `status`, `tanggal_lahir`, `tanggal_masuk`, `departemen`) VALUES ( 'Abi Isyawara', 'L', 'Belum', '1991-6-3', '2012-01-22', '3');
INSERT INTO `karyawan` ( `nama`, `jenis_kelamin`, `status`, `tanggal_lahir`, `tanggal_masuk`, `departemen`) VALUES ( 'Maman Kresna', 'L', 'Belum', '1993-8-21', '2012-09-15', '3');
INSERT INTO `karyawan` ( `nama`, `jenis_kelamin`, `status`, `tanggal_lahir`, `tanggal_masuk`, `departemen`) VALUES ( 'Nadia Aulia', 'P', 'Belum', '1989-10-7', '2012-05-07', '4');
INSERT INTO `karyawan` ( `nama`, `jenis_kelamin`, `status`, `tanggal_lahir`, `tanggal_masuk`, `departemen`) VALUES ( 'Mutiara Rezki', 'P', 'Menikah', '1988-3-23', '2013-05-21', '4');
INSERT INTO `karyawan` ( `nama`, `jenis_kelamin`, `status`, `tanggal_lahir`, `tanggal_masuk`, `departemen`) VALUES ( 'Dani Setiawan', 'L', 'Belum', '1986-2-11', '2014-11-30', '4');
INSERT INTO `karyawan` ( `nama`, `jenis_kelamin`, `status`, `tanggal_lahir`, `tanggal_masuk`, `departemen`) VALUES ( 'Budi Putra', 'L', 'Menikah', '1995-10-23', '2015-12-03', '4');
