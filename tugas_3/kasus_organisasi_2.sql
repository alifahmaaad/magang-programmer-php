
use sdm;

INSERT INTO `company` ( `nama`,`alamat`) VALUES ( 'PT JAVAN','Sleman');
INSERT INTO `company` ( `nama`,`alamat`) VALUES ( 'PT Dicoding','Bandung');


use sdm;

INSERT INTO `employee` ( `id`,`nama`, `atasan_id`, `company_id`) VALUES ('1','Pak Budi',NULL, '1');
INSERT INTO `employee` ( `id`,`nama`, `atasan_id`, `company_id`) VALUES ('2','Pak Tono','1', '1');
INSERT INTO `employee` ( `id`,`nama`, `atasan_id`, `company_id`) VALUES ('3','Pak Totok','1', '1');
INSERT INTO `employee` ( `id`,`nama`, `atasan_id`, `company_id`) VALUES ('4','Bu Sinta', '2','1');
INSERT INTO `employee` ( `id`,`nama`, `atasan_id`, `company_id`) VALUES ('5','Bu Novi', '3','1');
INSERT INTO `employee` ( `id`,`nama`, `atasan_id`, `company_id`) VALUES ('6','Andre', '4','1');
INSERT INTO `employee` ( `id`,`nama`, `atasan_id`, `company_id`) VALUES ('7','Dono', '4','1');
INSERT INTO `employee` ( `id`,`nama`, `atasan_id`, `company_id`) VALUES ('8','Ismir', '5','1');
INSERT INTO `employee` ( `id`,`nama`, `atasan_id`, `company_id`) VALUES ('9','Anto', '5','1');

SELECT nama FROM `employee` WHERE atasan_id is null ;

SELECT * FROM `employee` EXCEPT SELECT * FROM `employee` WHERE `id` IN (SELECT `atasan_id` FROM employee);

SELECT nama FROM `employee` WHERE atasan_id = 1 ;

SELECT * FROM `employee` except (SELECT * FROM `employee` EXCEPT SELECT * FROM `employee` WHERE `id` IN (SELECT `atasan_id` FROM employee)) EXCEPT (SELECT * FROM `employee` WHERE atasan_id = 1)EXCEPT (SELECT * FROM `employee` WHERE id = 1);

