<?php

use App\Http\Controllers\Tugas1Controller;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [Tugas1Controller::class, 'index']);
Route::get('/ganjilgenap', [Tugas1Controller::class, 'ganjilgenap'])->name('ganjilgenap');
Route::post('/ganjilgenappost', [Tugas1Controller::class, 'postganjilgenap']);
Route::get('/kalkulator', [Tugas1Controller::class, 'kalkulator'])->name('kalkulator');
Route::post('/kalkulatorpost', [Tugas1Controller::class, 'postkalkulator']);
Route::get('/hitungvokal', [Tugas1Controller::class, 'hitungvokal'])->name('hitungvokal');
Route::post('/hitungvokalpost', [Tugas1Controller::class, 'posthitungvokal']);
